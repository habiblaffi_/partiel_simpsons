import React, { useState, useEffect } from 'react';
import { SafeAreaView, FlatList, Platform, Text, View, StyleSheet, Image, ScrollView, TextInput, TouchableOpacity } from 'react-native';
import Randomquote from "./components/random-quote.js"
import Listquote from "./components/list-quotes.js";
import Charquote from "./components/character-quote.js";

export default function App() {
  const [quote, setQuote] = useState(null);
  const [character, setCharacter] = useState(null);
  const [image, setImage] = useState(null);
  const [listQuotes, setListQuotes] = useState(null);
  const [number, onChangeNumber] = React.useState('');
  const [charName, onChangeText] = React.useState('');
  const [charQuotes, setCharQuotes] = useState(null);

  function tacticalReload() {
    window.location.reload();
    // A CHANGER
  };

  const simpsons = () => {
    fetch(`https://thesimpsonsquoteapi.glitch.me/quotes`)
        .then(function(response){
          return response.json();
        }).then(function(response){
        console.log(response);
        setQuote(response[0].quote)
        setCharacter(response[0].character)
        setImage(response[0].image)
        })
  }

  const inputSimpsons = () => {
      console.log(number)
      fetch(`https://thesimpsonsquoteapi.glitch.me/quotes?count=${number}`)
          .then(function(response){
              return response.json();
          }).then(function(response){
          console.log(response);
          setListQuotes(response);
      })
  }

    const charSimpsons = () => {
        fetch(`https://thesimpsonsquoteapi.glitch.me/quotes?character=${charName}`)
            .then(function(response){
                return response.json();
            }).then(function(response){
            console.log(response);
            setCharQuotes(response);
        })
    }

  useEffect( () => {
    simpsons();
  }, [])


    return (
      <View style={styles.container}>
          <Randomquote
              image={image}
              nom={character}
              quote={quote} />
          <TouchableOpacity onPress={tacticalReload}><Text>Reload</Text></TouchableOpacity>

          <TextInput
              style={styles.input}
              onChangeText={onChangeNumber}
              value={number}
              placeholder="nombre de citations"
              keyboardType="numeric"
          />
          <TouchableOpacity onPress={inputSimpsons}><Text>Afficher liste</Text></TouchableOpacity>
          <FlatList
              data={listQuotes}
              renderItem={({item, index}) => {
                  return (<Listquote nom={item.character} quote={item.quote} image={item.image} key={index} />)
              }}
              keyExtractor={(item) => item.id}
          />

          <TextInput
              style={styles.input}
              onChangeText={onChangeText}
              value={charName}
              placeholder="rechercher un personnage"
          />
          <TouchableOpacity onPress={charSimpsons}><Text>Afficher citations</Text></TouchableOpacity>
          <FlatList
              data={charQuotes}
              renderItem={({item, index}) => {
                  return (<Charquote nom={item.character} quote={item.quote} image={item.image} key={index} />)
              }}
              keyExtractor={(item) => item.id}
          />
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  paragraph: {
    fontSize: 18,
    textAlign: 'center',
  },
  input: {
    border: '1px solid black',
  },
});
